# RUDP Java Library
### Simple library for creating and using non-blocking UDP sockets and connections with reliability mode

---

####**Introduction**

The basic idea of my work is create a lightweight library for the networking in the games development using only UDP.

Why it is really necessary?

1. Speed
2. Transmission efficiency
3. Min latency
4. Good for the small data

But we can a lose data, because it is a UDP :/

UDP doesn't have reliability.

There is no guarantee that the messages or packets sent would reach at all.

Because of it you can say that: "TCP is better, because it is reliable"
And you're right, but reliability of TCP has implications:

1. It is slower
2. Big latency
3. Bad for the small data
4. When packet is lose, sending another packets will stop before the lost packet will be delivered

And the 4 item is the main problem of using TCP in game development, especially in games of a dynamic genre(fps, racing, moba style, etc.)
Because in these types of the games you need to process only the actual data, you don't worry about where the player was 1 second ago, you are worried about whether the bullet hit the player at that moment or not.

If you are use TCP for the sending player position and lose one packet then programm will stop sending actual position before the lost packet with position will be delivered, but you don't need this position, because it is already old.
And you can get a big latency, about 0.1s.
It's really a lot!

For example:
```
fps in your game: 60
seconds per frame: 1s / 60 = 0.016
you will lose: 0.1 / 0.016 = 6.5 frames
You will see a 6.5 frames of sleep your game!
```

You can also say that you can use the TCP for the important data and UDP for not.
But this is a bad idea, because TCP and UDP are worked over IP and they can conflict on this level, who know how this conflict can be reflect on your application.

The solution is to realize TCP logic over UDP protocol. It is necessary that the UDP protocol can transmit data reliably when we want.
There are some standarts of the Reliable UDP
(
[rfc908](https://tools.ietf.org/html/rfc908), [rfc1151](https://tools.ietf.org/html/rfc1151)
)

My idea is to realize TCP logic over UDP and made a life of the game developer easier.

---

####**In the plans:**

* ~~Implement a base non-blocking UdpSocket system~~
* Implement pseudo connections for the sending reliable data
* Extend Non-blocking scheme to the asynchronous scheme(Give a choice for the developing)
* Solve the problem of 10k connections
* Using UdpSocket system for the STUN server architecture
* Using non-blocking UdpSocket system to work over NAT

---

#**Change log**

**Version 0.0.1**

Summary: 

- Simple non-blocking UdpSocket's
- UdpPacketReceiver interface

Simple non-blocking Server:

```java
public class Test {
	
	public static boolean run = true;

	public static void main(String[] args) throws IOException {
		// Creating a UDP Socket
		UdpSocket serverSocket = new UdpSocket("localhost", 3476);
		// Binding it and register a selector
		serverSocket.open();
		System.out.println("Server listened on " + serverSocket.getAddress());
		
		//Allocating buffer for all incoming packets
		ByteBuffer buffer = ByteBuffer.allocate(48);
		
		//Receiver interface for accepting incoming packets
		UdpPacketReceiver receiver = new UdpPacketReceiver(){
			
			//On accepting new packet
			@Override
			public void onPacket(DatagramChannel client) throws IOException {
				//Clearing buffer
				buffer.clear();
				
				//Getting client SocketAddress and reading data
				InetSocketAddress senderAddress = (InetSocketAddress) client.receive(buffer);
				System.out.println("Client: "+ senderAddress.getHostString() + "->" + senderAddress.getPort());
				
				//Convert buffer data to the String
				String output = new String(buffer.array()).trim().substring(0, buffer.position());
				
				//Print client message
				System.out.println("Msg: " + output + "\n");
				
				//If message equals to "bye" then close UdpSocket
				if (output.equals("bye")) {
					serverSocket.close();
                    System.out.println("Client messages are complete; close.");
                    run = false;
                }
			}
		};
		
		
		
		while(run){
			//If we have new packets then read it using UdpPacketReceiver
			if(serverSocket.hasMessages() == true){		
				serverSocket.getMessage(receiver);
			};
			
			//WHATEVER CODE YOU WANT
		}
		
	}

}
```

Simple non-blocking Client:

```java
public class TestClient {

	public static void main(String[] args) {
		// Creating a UDP Socket
		UdpSocket serverSocket = new UdpSocket(3500);
		// Binding it and register a selector
		serverSocket.open();
		
		byte[] message = new String("Hello World !!!").getBytes();
		byte[] message1 = new String("Hello World !!! Last").getBytes();
		byte[] message2 = new String("bye").getBytes();
		
		System.out.println("Socket listened on " + serverSocket.getAddress());
		
		// Address of the server
		InetSocketAddress client = new InetSocketAddress("localhost", 3476);
		
		//Sending messages to the server
		serverSocket.send(ByteBuffer.wrap(message), client);
		serverSocket.send(ByteBuffer.wrap(message1), client);
		serverSocket.send(ByteBuffer.wrap(message2), client);
		System.out.println("Send messages to " + client.getAddress());
		
		//Closing UDP Socket
		serverSocket.close();
		
		System.out.println("Socket closed");
		
	}

}
```

Client output:

```
Socket listened on 0.0.0.0/0.0.0.0:3500
Send messages to localhost/127.0.0.1
Socket closed
```

Server output:

```
Server listened on localhost/127.0.0.1:3476
Client: 127.0.0.1->3500
Msg: Hello World !!!

Client: 127.0.0.1->3500
Msg: Hello World !!! Last

Client: 127.0.0.1->3500
Msg: bye

Client messages are complete; close.
```
---
