package es.rudp.core;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

public class Test {
	
	public static boolean run = true;

	public static void main(String[] args) throws IOException {
		// Creating a UDP Socket
		UdpSocket serverSocket = new UdpSocket("localhost", 3476);
		// Binding it and register a selector
		serverSocket.open();
		System.out.println("Server listened on " + serverSocket.getAddress());
		
		//Allocating buffer for all incoming packets
		ByteBuffer buffer = ByteBuffer.allocate(48);
		
		//Receiver interface for accepting incoming packets
		UdpPacketReceiver receiver = new UdpPacketReceiver(){
			
			//On accepting new packet
			@Override
			public void onPacket(DatagramChannel client) throws IOException {
				//Clearing buffer
				buffer.clear();
				
				//Getting client SocketAddress and reading data
				InetSocketAddress senderAddress = (InetSocketAddress) client.receive(buffer);
				System.out.println("Client: "+ senderAddress.getHostString() + "->" + senderAddress.getPort());
				
				//Convert buffer data to the String
				String output = new String(buffer.array()).trim().substring(0, buffer.position());
				
				//Print client message
				System.out.println("Msg: " + output + "\n");
				
				//If message equals to "bye" then close UdpSocket
				if (output.equals("bye")) {
					serverSocket.close();
                    System.out.println("Client messages are complete; close.");
                    run = false;
                }
			}
		};
		
		
		
		while(run){
			//If we have new packets then read it using UdpPacketReceiver
			if(serverSocket.hasMessages() == true){		
				serverSocket.getMessage(receiver);
			};
			
			//WHATEVER CODE YOU WANT
		}
		
	}

}
