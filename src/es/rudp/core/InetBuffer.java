package es.rudp.core;

public class InetBuffer {
	
	protected int size;
	protected byte[] buffer;
	protected int pos = 0;
	
	protected int iter = 0;
	
	protected int[] intMask = new int[]{
			0xFF000000, 
			0x00FF0000,
			0x0000FF00,
			0x000000FF
		};
	
	protected int[] shortMask = new int[]{
			0xFF00, 
			0x00FF
		};
	
	public InetBuffer(int size) {
		buffer = new byte[size];
		this.size = size;
	}
	
	private int getNumberFromBuffer(int position, int capacity){
		int result = 0;
		for (iter = 0; iter < capacity; iter++) {
			result += (int)((buffer[position + iter] + 128) << ((capacity - iter - 1) * 8));
		}
		return result;
	}
	
	public void putInt4(int number){
		for (iter = 1; iter <= 4; iter++) {
			buffer[pos] = (byte)(((number & intMask[iter - 1]) >> ((4 - iter) * 8)) - 128);
			pos++;
		}
	}
	
	public void putInt2(int number){
		for (iter = 1; iter <= 2; iter++) {
			buffer[pos] = (byte)(((number & shortMask[iter - 1]) >> ((2 - iter) * 8)) - 128);
			pos++;
		}
	}
	
	public void putInt1(int number){
		buffer[pos] = (byte)(number - 128);
		pos++;
	}
	
	public void putShort(short number){
		for (iter = 1; iter <= 2; iter++) {
			buffer[pos] = (byte)(((number & shortMask[iter - 1]) >> ((2 - iter) * 8)) - 128);
			pos++;
		}
	}
	
	public void putByte(byte number){
		buffer[pos] = (byte) (number - 128);
		pos++;
	}
	
	public int getInt4(int position){
		return getNumberFromBuffer(position, 4);
	}
	
	public int getInt2(int position){
		return getNumberFromBuffer(position, 2);
	}
	
	public int getInt1(int position){
		return getNumberFromBuffer(position, 1);
	}
	
	public byte getByte(int position){
		return (byte) getNumberFromBuffer(position, 1);
	}
	
	public short getShort(int position){
		return (short) getNumberFromBuffer(position, 2);
	}
	
	public byte[] getByteArray(){
		return buffer;
	}
	
	/*
	 * for (int i = 0; i < 4; i++) {
		    bytes[i] = (byte)(integer >>> (i * 8));
		}
	 */
}
