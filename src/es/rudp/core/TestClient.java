package es.rudp.core;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

public class TestClient {

	public static void main(String[] args) {
		// Creating a UDP Socket
		UdpSocket serverSocket = new UdpSocket(3500);
		// Binding it and register a selector
		serverSocket.open();
		
		byte[] message = new String("Hello World !!!").getBytes();
		byte[] message1 = new String("Hello World !!! Last").getBytes();
		byte[] message2 = new String("bye").getBytes();
		
		System.out.println("Socket listened on " + serverSocket.getAddress());
		
		// Address of the server
		InetSocketAddress client = new InetSocketAddress("localhost", 3476);
		
		//Sending messages to the server
		serverSocket.send(ByteBuffer.wrap(message), client);
		serverSocket.send(ByteBuffer.wrap(message1), client);
		serverSocket.send(ByteBuffer.wrap(message2), client);
		System.out.println("Send messages to " + client.getAddress());
		
		//Closing UDP Socket
		serverSocket.close();
		
		System.out.println("Socket closed");
		
		InetBuffer buff = new InetBuffer(16);
		buff.putInt4(2147483647);
		buff.putInt2(65535);
		buff.putShort((short) 29878);
		buff.putShort((short) 32000);
		buff.putByte((byte) 127);
		buff.putInt1(255);
		
		//byte[] test = buff.getByteArray();
		//for(int i=0; i<test.length; i++) System.out.println(test[i]);
		
		
		System.out.println(buff.getInt4(0));
		//System.out.println((short) 32098);
		System.out.println(buff.getInt2(4));
		System.out.println(buff.getShort(6));
		System.out.println(buff.getShort(8));
		System.out.println(buff.getByte(10));
		System.out.println(buff.getInt1(11));
	}

}
