package es.rudp.core;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.util.Set;

public class UdpSocket {
	
	protected int PORT = 6404;
	protected String HOST = "";
	protected InetSocketAddress address;
	
	protected DatagramChannel channel;
	protected int validOperations;
	protected SelectionKey selectKey;
	protected Selector selector;
	
	public UdpSocket(){
		this.address = new InetSocketAddress(this.PORT);
	}
	
	public UdpSocket(int port){
		this.PORT = port;
		this.address = new InetSocketAddress(this.PORT);
	}
	
	public UdpSocket(String host, int port){
		this.HOST = host;
		this.PORT = port;
		this.address = new InetSocketAddress(this.HOST, this.PORT);
	}
	
	public void open(){
		try {
			channel = DatagramChannel.open();
			channel.socket().bind(this.address);
			channel.configureBlocking(false);
			
			selector = Selector.open();
			
			validOperations = channel.validOps();
			selectKey = channel.register(selector, validOperations, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void send(ByteBuffer buffer, SocketAddress address){
		try {
			channel.send(buffer, address);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean hasMessages(){
		int selectedSize = 0;
		try {
			selectedSize = selector.selectNow();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return (selectedSize > 0);
	}
	
	public void getMessage(UdpPacketReceiver receiver) throws IOException{
		Set<SelectionKey> keys = selector.selectedKeys();
        Iterator<SelectionKey> iter = keys.iterator();

        while (iter.hasNext()) {

            SelectionKey ky = iter.next();

            if (ky.isReadable()) {

                // Read the data from client
            	DatagramChannel client = (DatagramChannel) ky.channel();
            	receiver.onPacket(client);              
            } 
        }
	}
	
	public int getPort(){
		return this.PORT;
	}
	
	public String getHost(){
		return this.HOST;
	}
	
	public InetSocketAddress getAddress(){
		return this.address;
	}
	
	public void close(){
		try {
			channel.socket().close();
			channel.close();
			selector.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
