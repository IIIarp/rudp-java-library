package es.rudp.core;

import java.nio.ByteBuffer;

public class RDProtocolPacket {
	
	public ByteBuffer ProtocolBuffer;
	
	public RDProtocolPacket(){
		ProtocolBuffer = ByteBuffer.allocate(RDProtocolBuffer.BUFFER_SIZE);
	}
	
	public void setFlagData(byte FLAG, byte[] data, byte LENGTH){
		ProtocolBuffer.put(data, FLAG, LENGTH);
	}
	
	public void resetBuffer(){
		RDProtocolBuffer.resetBuffer(ProtocolBuffer);
	}
	
	public int collectChecksum(){
		//Short.toUnsignedInt()
		ProtocolBuffer.clear();
		int summ = 0;
		
		for(int i=0; i<RDProtocolBuffer.BUFFER_SIZE / 16; i++){
			summ += Short.toUnsignedInt(ProtocolBuffer.getShort(i*16));
			summ = (summ >> 16) + (summ & 0xffff);
		}
		
		
		System.out.println(summ);
		System.out.println(0xffff - summ);
		
		return 0xffff - summ;
	}
}
