package es.rudp.core;

import java.io.IOException;
import java.nio.channels.DatagramChannel;

public interface UdpPacketReceiver{
	public void onPacket(DatagramChannel client) throws IOException;
}
