package es.rudp.core;

import java.nio.ByteBuffer;

public abstract class RDProtocolBuffer {
	
	public static final byte FLAG_SYN = 0x0;
	public static final byte FLAG_ACK = 0x1;
	public static final byte FLAG_EAK = 0x2;
	public static final byte FLAG_RST = 0x3;
	public static final byte FLAG_NUL = 0x4;
	public static final byte FLAG_CHK = 0x5;
	public static final byte FLAG_NCK = 0x6;
	public static final byte FLAG_ZERO = 0x7;
	public static final byte FLAG_HEADER_LENGTH = 0x8;
	public static final byte FLAG_SEQUENCE_NUMBER = 0x10;
	public static final byte FLAG_ASK_NUMBER = 0x18;
	public static final byte FLAG_CHECKSUM = 0x20;
	
	public static final byte LENGTH_SYN = 0x1;
	public static final byte LENGTH_ASK = 0x1;
	public static final byte LENGTH_EAK = 0x1;
	public static final byte LENGTH_RST = 0x1;
	public static final byte LENGTH_NULL = 0x1;
	public static final byte LENGTH_CHK = 0x1;
	public static final byte LENGTH_NCK = 0x1;
	public static final byte LENGTH_ZERO = 0x1;
	public static final byte LENGTH_HEADER = 0x8;
	public static final byte LENGTH_SEQUENCE_NUMBER = 0x8;
	public static final byte LENGTH_ASK_NUMBER = 0x8;
	public static final byte LENGTH_CHECKSUM = 0x10;
	
	public static final byte BUFFER_SIZE = 0x30;
	
	public static byte[] ZeroData = new byte[BUFFER_SIZE];
	public static ByteBuffer shortBuffer = ByteBuffer.allocate(2);
	
	public static void resetBuffer(ByteBuffer ProtocolBuffer){
		ProtocolBuffer.put(ZeroData);
		ProtocolBuffer.clear();
	}
	
	public static byte[] getShortBytes(short value){
		shortBuffer.putShort(value);
		return shortBuffer.array();
	}
}
